package com.example.asyntask;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new MyAsyncTask().execute("https://jsonplaceholder.typicode.com/albums");
    }
    class MyAsyncTask extends AsyncTask<String, Integer, String>{
        private static final String TAG = "ooooo";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e(TAG, "OnPreExecute");
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("GET");

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

                int c;
                while ((c = bufferedReader.read()) != -1) {
                    result += String.valueOf((char) c);
                }

                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {

                JSONArray jsonArray = new JSONArray(result);
                JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                String title = jsonObject1.getString("title");

                Log.d(TAG,"title="+ title);

                /*
                JSONObject object = new JSONObject(result);
                JSONArray data = object.getJSONArray("DATA");
                object = data.getJSONObject(1);
                object = object.getJSONObject("CATEGORY");
                Log.e(TAG, object.getInt("ID") + "");

                 */
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
